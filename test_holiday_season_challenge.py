import unittest
from holiday_season_challenge import DynamicProgrammingSolver


class TestDynamicProgrammingSolver(unittest.TestCase):
    def test_no_character(self):
        self.given_string('')
        self.assert_solutions_count(0)

    def test_three_characters(self):
        self.given_string('ulu')
        self.assert_solutions_count(0)

    def test_one_solution(self):
        self.given_string('yoyo')
        self.assert_solutions_count(1)

    def test_two_solutions(self):
        self.given_string('hehee')
        self.assert_solutions_count(2)

    def test_many_solutions(self):
        self.given_string('ababbba')
        self.assert_solutions_count(7)

    def given_string(self, string):
        self.solver = DynamicProgrammingSolver()
        for character in string:
            self.solver.feed_character(character)

    def assert_solutions_count(self, expected):
        self.assertEqual(expected, self.solver.solutions_count)



