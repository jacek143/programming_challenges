def compute(string):
    solver = DynamicProgrammingSolver()
    for character in string:
        solver.feed_character(character)
    return solver.solutions_count


class DynamicProgrammingSolver:
    def __init__(self):
        self.__sub_solutions = {'': 1}

    def __get_sub_solutions_count(self, solution_string):
        return self.__sub_solutions.get(solution_string, 0)

    def feed_character(self, character):
        keys_to_update = self.__get_keys_to_update(character)
        sub_solutions_to_update = self.__get_new_sub_subsolutions(keys_to_update)
        self.__sub_solutions.update(sub_solutions_to_update)

    def __get_new_sub_subsolutions(self, keys_to_update):
        sub_solutions_to_update = dict()
        for key in keys_to_update:
            sub_solutions_to_update[key] = self.__get_sub_solutions_count(key) + self.__get_sub_solutions_count(key[:-1])
        return sub_solutions_to_update

    def __get_keys_to_update(self, character):
        keys_to_update = set()
        for key in self.__sub_solutions:
            candidate = key + character
            candidate_length = len(candidate)
            if candidate_length in [1, 2]:
                keys_to_update.add(candidate)
            elif candidate_length == 3:
                if candidate[0] == candidate[2]:
                    keys_to_update.add(candidate)
            elif candidate_length == 4:
                if candidate[0] == candidate[2] and candidate[1] == candidate[3]:
                    keys_to_update.add(candidate)
        return keys_to_update

    @property
    def solutions_count(self):
        return sum([value for key, value in self.__sub_solutions.items() if len(key) == 4])


if __name__ == '__main__':
    N = int(input())
    characters = input()
    assert N == len(characters)
    print(compute(characters))
